
import os.path

import sys

import matplotlib.pyplot as plt
import matplotlib as mpl

import pandas


mpl.rcParams.update({'font.size': 10})

def get_data(file_list):

    per_histo_list = []
    histo_dict = {}
    for file in file_list:

        data = pandas.read_csv(
            direc + '/' + file,
            header=0,
            names=['bin start', 'count'],
            sep=',')
        gs = data['bin start']
        count = data['count']

        gs = gs.tolist()
        count = count.tolist()
        per_histo_list.append(gs)
        per_histo_list.append(count)
        # print(per_histo_list)
        histo_dict[file[:-4]] = per_histo_list
        per_histo_list = []

    return histo_dict

def get_total_volume(fileloc):

    data = pandas.read_csv(fileloc, header=0, sep='\t')
    #print(data)
    tv_dict = {}  # format of: spec name = total volume
    tv_array = data['TV (pixels)']
    for spec, tv in zip(data['Specimen'],data['TV (pixels)']):
        tv_dict[spec[:-8]] = tv
        print(spec[:-8])

    #print("tv_dict = " + str(tv_dict))
    return tv_dict


def plot(data, pervol, tv_dict):

    volume_dict = {}
    for a in data.keys():
        volume_dict[a] = 0
    for a in data.keys():   #find tot volume
        for c in data[a][1]:
            volume_dict[a] += c

        print(a + '= ' + str(volume_dict[a]))


    highestArr = [0 for i in range(0,256)]
    lowestArr = [10000000 for i in range(0,256)]
    x = range(0,256)
    for a in data.keys(): #find highest
        if not 'PC' in a:

            cnt =0
            for count in data[a][1]:
                count = count/volume_dict[a]
                if(count > highestArr[cnt]):
                    highestArr[cnt] =count

                if(count < lowestArr[cnt]):
                    lowestArr[cnt] =count
                cnt+=1

    #print(highestArr)
    #print(lowestArr)





    for a in data.keys():
        data[a][1][:] = [x/volume_dict[a] for x in data[a][1]]


    fig = plt.figure()
    # fig.suptitle('Histograms', fontsize=22)
    ax = fig.add_axes([0.1, 0.1, 0.6, 0.75])
    ax.set_yscale('log')
    count = 0

    for a in data.keys():


        divby = 1
        print(str(a) + "\n\n\n\n\n")
        #print(pervol)
        if (pervol == 'True'):
            for spec in tv_dict.keys():
                if spec in a:
                    #print(spec)
                    divby = tv_dict[spec]
                    #print(str(divby))
                    i = 0
                    for val in data[a][1]:
                        #print("oldval = " + str(val))
                        val = val/divby
                        #print("newval = " + str(val))
                        #print("before = " + str(data[a][1][i]))
                        data[a][1][i] = val
                        #print("after = " + str(data[a][1][i]))
                        i+=1
                    break

        if 'PC' in a:

            ax.plot(
                data[a][0],
                data[a][1],
                label=a[2:-30].replace('_',' '),
                linewidth=2)
        # print(data[i * per_spec + a][2])
        # print('i = ' + str(i) + ', a = ' + str(a))
        count+=1

    ax.fill_between(x, highestArr, lowestArr, alpha = 0.5, color='grey')
    ax.plot(
            x,
            lowestArr,
            #label='low',
            linewidth=1,
            color='grey',
            alpha = 0.2)
    ax.plot(
            x,
            highestArr,
            #label='high',
            linewidth=1,
            color='grey',
            alpha = 0.2)

    ax.legend()
   # ax.legend(
   #     bbox_to_anchor=(1.05, 1.),
   #     loc=2,
   #     borderaxespad=0.,
   #     fontsize=9)
    plt.xlabel('Greyscale Value')
    plt.ylabel('Normalised Count')
    plt.xlim([0, 255])
    plt.ylim([0.00005,0.05])
    plt.savefig('pca1_histo.pdf', dpi=320, facecolor='w', edgecolor='w',
        orientation='portrait', format=None,
        transparent=True, bbox_inches='tight', pad_inches=0.2,
        frameon=None)

    #plt.show()
    plt.close()



if __name__ == "__main__":

    #mpl.style.use('ggplot')

    direc = sys.argv[1]
    pervol = sys.argv[2]
    fileType = sys.argv[3]

    if pervol == 'True':
        tv_data = get_total_volume('/Users/fraun/Documents/Backgrounds/Results_FROM_ROI.csv')
    else:
        tv_data = {}
    list_of_csvs = []
    # Scan through them separating them.
    list_of_files = sorted(os.listdir(direc))
    for file in list_of_files:
        if str(fileType) in file:
            list_of_csvs.append(file)
            print(file)
    data_dict = get_data(list_of_csvs)
    plot(data_dict, pervol, tv_data)
