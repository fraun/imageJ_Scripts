
import os.path

import sys



if __name__ == "__main__":

	list_of_files = sorted(os.listdir(sys.argv[1]))
	os.chdir(sys.argv[1])
	for file in list_of_files:
		if 'xls' in file:
			print(file)
			newlines = []
			with open(file,'r') as f:
				lines = f.readlines()
			
			#print(newlines)
			for line in lines:
				line = line.replace("\t", ", ")
				newlines.append(line)
			#print(newlines)
			with open(file+'.csv', 'w+')as wfile:
				wfile.writelines(newlines)

