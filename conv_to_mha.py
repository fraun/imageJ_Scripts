import os
import shutil
from ij import IJ



def edit_in_imj(folder, file_dir):

    list_of_files = sorted(os.listdir(file_dir + "\\" + folder))

    imp = IJ.run(
        "Image Sequence...",
        "open=[" +
        file_dir +
        "\\" +
        folder +
        "\\" +
        list_of_files[0] +
        "] sort")

    print(file_dir +
        "\\" +
        folder)

    IJ.run(
        imp,
        "MHD/MHA ...",
        "save=" +
        file_dir +
        "\\" +
        folder)

    
    IJ.run("Close")

if __name__ == "__main__":

    fileDir = 'F:\\PHD\\HUMAN\\Intact\\Scan_Data\\ALL'

    os.chdir(fileDir)
    # Scan through them separating them.
    listOfFolderNames = sorted(os.listdir(fileDir))
    
    for folder in listOfFolderNames:
        edit_in_imj(folder, fileDir)
    print("Done")
