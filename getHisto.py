from ij import IJ
import os

# ----------------------------------------------------------------
fileDir = 'F:\\PHD\\HUMAN\\PCA\\Generated VBs\\Backgrounds'
# ----------------------------------------------------------------


def editInImJ(folder, fileDir):

    listOfFiles = sorted(os.listdir(fileDir + "\\" + folder))
    mid_slice = len(listOfFiles)/2
    print(listOfFiles)
    print("open=[" +
        fileDir +
        "\\" +
        folder +
        "\\" +
        listOfFiles[0])
        
    imp = IJ.run(
        "Image Sequence...",
        "open=[" +
        fileDir +
        "\\" +
        folder +
        "\\" +
        listOfFiles[0] +
        "]" +
        " sort")

    
    imp = IJ.getImage()
   
    stats = imp.getStatistics()
    IJ.run(imp, "Histogram", "stack")
    IJ.saveAs("Results", fileDir + "\\Histogram"+folder+".xls")
    imp = IJ.getImage()
    IJ.run(imp, "Save", "save=[" + fileDir +
           "\\Histogram"+folder+".png]")

    
    IJ.run("Close")
    IJ.run("Close")

def get_histo_roi(folder, files):

    imp = IJ.openImage(folder + files)
    # imp = IJ.getImage()
    stats = imp.getStatistics()
    IJ.run(imp, "Histogram", "stack")
    imp = IJ.getImage()
    IJ.run(imp, "Save", "save=[" + folder +
           "\\Histogram_of_" + files + "]")

    GS = 0
    print(stats.histogram[0:])
    fil = open(folder + "\\" +
               files[:-5] + '_histogram.csv', 'w')
    # print(fileDirF + "\\" + listOfFiles[mid_slice][:-5] + '_histogram.csv')
    for i in stats.histogram[0:]:
        fil.write(str(GS) + ', ' + str(i) + '\n')
        GS += 1
    fil.close()
    IJ.run("Close")


if __name__ == "__main__":

    os.chdir(fileDir)
    # Scan through them separating them.
    listOfFolderNames = sorted(os.listdir(fileDir))
    print(listOfFolderNames)
    for folder in listOfFolderNames:
        if not "istogram" in folder:
            print(folder)
            editInImJ(folder, fileDir)
            #get_histo_roi(fileDir, folder)